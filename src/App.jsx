import { useState, useEffect } from "react";
import {
  Navbar,
  Nav,
  Container,
  Button,
  Form,
  Col,
  Row,
} from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";
// import wallpaper from "./img/wallpaper.jpg";
import Slideshow from "./Slideshow";
import "./App.css";
import UserContext from "./UserContext";
import { AiOutlineShoppingCart, AiOutlineHome } from "react-icons/ai";

function App() {
  const articulos = [
    { id: 1, url: "https://placeimg.com/400/600", categoria: "A" },
    { id: 2, url: "https://placeimg.com/400/600", categoria: "A" },
    { id: 3, url: "https://placeimg.com/400/600", categoria: "B" },
    { id: 4, url: "https://placeimg.com/400/600", categoria: "B" },
    { id: 5, url: "https://placeimg.com/400/600", categoria: "C" },
    { id: 6, url: "https://placeimg.com/400/600", categoria: "B" },
  ];

  const formatos = [
    { id: 1, formato: "20x30", precio: 24.99 },
    { id: 2, formato: "30x40", precio: 34.99 },
    { id: 3, formato: "40x50", precio: 49.99 },
    { id: 4, formato: "40x60", precio: 54.99 },
    { id: 5, formato: "50x75", precio: 69.99 },
    { id: 6, formato: "60x80", precio: 79.99 },
    { id: 7, formato: "60x90", precio: 84.99 },
    { id: 8, formato: "75x100", precio: 99.99 },
  ];

  const [carrito, setCarrito] = useState([]);
  const [precioFinal, setPrecioFinal] = useState(0);

  console.log(carrito);

  // esta función estará a nivel global poorque quiero que sirva para todos los componentes hijos. aquí se hará la operación que resetea el precio final cada vez que cambie/se añade o borre un elemento del carrito(por consiguiente se borra o añade un precio).

  function sumaTotalCarrito() {
    let total = 0;
    carrito.forEach((el) => {
      total += Number(el.total_sel); //Number convierte el valor a n'umero. A veces esto lee los n'umeros como strings y en vez de sumar 13+12=25, suma 1312. para evitar esto, me puedo asegurar de que lee como n'umero usando un constructor Number.
    });
    setPrecioFinal(total.toFixed(2)); //toFixed(2) se encarga de mostrar la cantidad de decimales que le pida entre par'entesis.
  }

  ////meto la función de suma total en un useEffect global del App para que cada vez que se cambie algo del carrito esté en el modal que esté, se ejecute automáticamente un recálculo de la suma y se actualice el precio que está al lado del icono carrito.

  ///En un useEffect pido que se ejecute automáticamente la suma de los total_sel de cada elemento del carrito (función de arriba sumaTotalCarrito) cada vez que haya una actualización en la variable carrito.
  useEffect(() => {
    sumaTotalCarrito();
  }, [carrito]);

  function eliminar(pedido) {
    setCarrito(carrito.filter((el) => el.total_sel !== pedido.total_sel));
    setPrecioFinal(sumaTotalCarrito - pedido.total_sel);
  }

  return (
    <div>
      {/* con el PROVIDER envuelvo todas las etiquetas que recibirán estos valores. incluido el OUTLET, que cuenta con todas las rutas que le he indicado */}
      <UserContext.Provider
        value={{
          articulos,
          formatos,
          setCarrito,
          carrito,
          precioFinal,
          setPrecioFinal,
          eliminar,
        }}
      >
        <Container fluid>
          <Row>
            <Navbar bg="dark" variant="dark" expand="lg">
              {" "}
              <Link className="nav-link" to="/">
                <i>
                  <AiOutlineHome />
                </i>
              </Link>
              <div className="icono-carrito">
                <Link to="factura">
                  <span>
                    <AiOutlineShoppingCart />
                  </span>
                  <span style={{ "font-size": "1rem" }}> {precioFinal} €</span>
                </Link>
              </div>
            </Navbar>
          </Row>

          {/* ABRE SEGUNDO ROW. COL IZQUIERDA FIJA. COL DERECHA OUTLET CON RUTAS. */}
          <Row>
            <Col md="5">
              <div className="big-box">
                <Slideshow />
              </div>
            </Col>
            <Col md="7">
              <div className="big-box">
                <Outlet />
              </div>
            </Col>
          </Row>
        </Container>
      </UserContext.Provider>
    </div>
  );
}

export default App;
